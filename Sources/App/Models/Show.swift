//
//  Show.swift
//  
//
//  Created by Ataias Pereira Reis on 04/12/21.
//

import Fluent
import Vapor

final class Show: Model, Content {
    static let schema = "shows"

    @ID(key: .id)
    var id: UUID?

    @Field(key: "title")
    var title: String

    @Field(key: "releaseYear")
    var releaseYear: Int

    @Children(for: \.$show)
    var reviews: [Review]

    init() {}

    init(id: UUID? = nil, title: String, releaseYear: Int) {
        self.id = id
        self.title = title
        self.releaseYear = releaseYear
    }
}

extension Show {
    func getReviews(
        request: Request,
        arguments: PaginationArguments
    ) throws -> EventLoopFuture<[Review]> {
        $reviews.query(on: request.db)
            .limit(arguments.limit)
            .offset(arguments.offset)
            .all()
    }
}
