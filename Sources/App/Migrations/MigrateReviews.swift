//
//  MigrateReviews.swift
//  
//
//  Created by Ataias Pereira Reis on 04/12/21.
//

import Fluent

struct MigrateReviews: AsyncMigration {

     func prepare(on database: Database) async throws {

        try await database.schema("reviews")
            .id()
            .field("title", .string, .required)
            .field("text", .string, .required)
            .field("show_id", .uuid, .required, .references("shows", "id"))
            .create()

        let shows = try await Show.query(on: database).all()
        let reviews = shows.map {
            Review(showID: $0.id!, title: "A new review for show \($0.title)", text: "This was an awesome show")
        }

        reviews.forEach { review in
            _ = review.save(on: database)
        }
    }

    func revert(on database: Database) async throws  {
        return try await database.schema("reviews").delete()
    }
}
