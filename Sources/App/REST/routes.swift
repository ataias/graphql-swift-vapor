//
//  routes.swift
//  
//
//  Created by Ataias Pereira Reis on 04/12/21.
//


import Vapor

func routes(_ app: Application) throws {
    app.get("hello") { req -> String in
        return "Hello, world!"
    }

    app.get("allShows") { req async throws -> [Show] in
        try await Show.query(on: app.db)
                .all()

    }
}
