//
//  MigrateShows.swift
//  
//
//  Created by Ataias Pereira Reis on 04/12/21.
//

import Fluent

struct MigrateShows: AsyncMigration {
    func prepare(on database: Database) async throws {
        try await database.schema("shows")
            .id()
            .field("title", .string, .required)
            .field("releaseYear", .int, .required)
            .create()

        let show1 = Show(title: "The first show", releaseYear: 2021)
        let show2 = Show(title: "The second show", releaseYear: 2023)
        let show3 = Show(title: "The third show", releaseYear: 2025)

        try await show1.save(on: database)
        try await show2.save(on: database)
        try await show3.save(on: database)
    }
    
    func revert(on database: Database) async throws {
        return try await database.schema("shows").delete()
    }
}
