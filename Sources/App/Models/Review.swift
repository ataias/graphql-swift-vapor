//
//  Review.swift
//  
//
//  Created by Ataias Pereira Reis on 04/12/21.
//

import Fluent
import Vapor

final class Review: Model, Content {
    static let schema = "reviews"

    @ID(key: .id)
    var id: UUID?

    @Field(key: "title")
    var title: String

    @Field(key: "text")
    var text: String

    @Parent(key: "show_id")
    var show: Show

    init() { }

    init(id: UUID? = nil, showID: UUID, title: String, text: String) {
        self.id = id
        self.$show.id = showID
        self.title = title
        self.text = text
    }
}
