# GraphQL Swift Vapor Example

A GraphQL example following a [Raywenderlich tutorial](https://www.raywenderlich.com/21148796-graphql-tutorial-for-server-side-swift-with-vapor-getting-started).

It differs from the tutorial in some minor aspects:

- Added database seed
- Added postgres (for now, need to comment/uncomment line in configure if you prefer that option)
- Upgraded dependencies
  - Vapor with async/await; used it for seeding database
  - Can't use async/await in resolvers yet 
- Added an example of how to document a field (see `Schema.swift`)
- Added CORS configuration to support Apollo Studio
